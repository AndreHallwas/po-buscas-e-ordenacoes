package Tabela;

import java.io.RandomAccessFile;
import java.io.IOException;
import java.io.File;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

//... classe Entrada (possui m�todos para entrada de dados) ....
class Entrada
{
    public static String leString(String msg)
    {
        String line = "";
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        try
        {
            System.out.println(msg);
            line = br.readLine();
        } catch (Exception e)
        {  }
        return line;
    }

    public static int leInteger(String msg)
    {
        String line = "";
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        try
        {
            System.out.println(msg);
            line = br.readLine();
            int retorno = Integer.valueOf(line);
            return retorno;
        } catch (Exception e)
        {  return -1; }
    }
}

//... classe Arquivo (onde vai estar o m�todo para ordernar, etc) ....
public class Arquivo_Java
{
    public String nomearquivo;
    public RandomAccessFile arquivo;
    public long compP,compEq,movP,movEq,Tempo;

    public Arquivo_Java(String nomearquivo)
    {
        try
        {
            arquivo = new RandomAccessFile(nomearquivo, "rw");
            this.nomearquivo = nomearquivo;
            compP=compEq=movP=movEq=Tempo = 0;
        } catch (IOException e)
        { }
    }

    public void truncate(long pos) //desloca eof
    {
        try
        {
            arquivo.setLength(pos * Registro.length());
        } catch (IOException exc)
        { }
    }

    //semelhante ao feof() da linguagem C
    //verifica se o ponteiro esta no <EOF> do arquivo
    public boolean eof()  
    {
        boolean retorno = false;
        try
        {
            if (arquivo.getFilePointer() == arquivo.length())
                retorno = true;                               
        } catch (IOException e)
        { }
        return (retorno);
    }

    //insere um Registro no final do arquivo, passado por par�metro
    public void inserirRegNoFinal(Registro reg)
    {
        try {
            seekArq((int) arquivo.length());//ultimo byte
        } catch (IOException ex) {
            Logger.getLogger(Arquivo_Java.class.getName()).log(Level.SEVERE, null, ex);
        }
        reg.gravaNoArq(arquivo);
    }

    public void exibirArq()
    {
        int i;
        Registro aux = new Registro();
        seekArq(0);
        i = 0;
        while (!this.eof())
        {
            /////System.out.println("Posicao " + i);
            aux.leDoArq(arquivo);
            aux.exibirReg();
            i++;
        }
    }

    public void exibirUmRegistro(int pos)
    {
        Registro aux = new Registro();
        seekArq(pos);
        System.out.println("Posicao " + pos);
        aux.leDoArq(arquivo);
        /////aux.exibirReg();
    }

    public void seekArq(int pos)
    {
        try
        {
            arquivo.seek(pos * Registro.length());
        } catch (IOException e)
        { }
    }
    
    public long filesize(){
        long aux = 0;
        try {
            aux =  arquivo.length()/Registro.length();
        } catch (IOException ex) {
            Logger.getLogger(Arquivo_Java.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return aux;
    }

    public void leArq()
    {    }
    
    public void geraArqOrdenado(){
        Registro aux ;
        for (int i = 0; i < 250; i++) {
            aux = new Registro(i);
            aux.gravaNoArq(arquivo);
        }
    }
    
    public void geraArqReverso(){
        Registro aux ;
        for (int i = 250; i > 0; i--) {
            aux = new Registro(i);
            aux.gravaNoArq(arquivo);
        }
    }
    
    public void geraArqRandomico(){
        Registro aux;
        Random ran = new Random();
        for (int i = 0; i < 250; i++) {
            aux = new Registro(ran.nextInt(1000));
            aux.gravaNoArq(arquivo);
        }
    }
    
    public void copiarArquivo(String outp) {
        try {
            FileInputStream in = new FileInputStream(nomearquivo);
            FileOutputStream out = new FileOutputStream(outp);
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //.............................................................................
    /*

    insira aqui os m�todos de Ordena��o;

    */
    public void executa()
    {
        /////leArq();
        exibirArq();
    }
    
    public void geraRelatorio(){
        
    }

    //m�todo principal
    public static void main(String args[])
    {
        Arquivo_Java a = new Arquivo_Java("arquivo.dat");
        a.geraArqReverso();
        a.executa();
    }
}

class ordenacao_Arquivo extends Arquivo_Java{

    public ordenacao_Arquivo(String nomearquivo) {
        super(nomearquivo);
    }
    
    public void insercaoDireta() throws IOException{
       Registro aux = new Registro();
       Registro reg = new Registro();
       int j,tl = (int) filesize();
        for (int i = 1; i < tl; ++i) {
            seekArq(i);
            aux.leDoArq(arquivo);
            j = i-1;
            seekArq(j);
            reg.leDoArq(arquivo);
            while(j >= 0 && aux.getCodigo() < reg.getCodigo()){
                compP++;
                seekArq(j+1);
                reg.gravaNoArq(arquivo);
                j--;
                seekArq(j);
                reg.leDoArq(arquivo);
                movP++;
                
            }
            seekArq(j+1);
            movP++;
            aux.gravaNoArq(arquivo);
        }
    }
    
     public void insercaoDiretaBinaria() throws IOException{
       Registro aux = new Registro();
       Registro reg = new Registro();
       Registro loc = new Registro();
       
       int j,tl = (int) filesize();
        for (int i = 1; i < tl; ++i) {
            seekArq(i);
            aux.leDoArq(arquivo);
            j = i-1;
            seekArq(j);
            reg.leDoArq(arquivo);
            seekArq(buscaBinaria(aux,0,j));
            loc.leDoArq(arquivo);
            while(reg.getCodigo() >= loc.getCodigo()){
                compP++;
                seekArq(j+1);
                reg.gravaNoArq(arquivo);
                j--;
                seekArq(j);
                reg.leDoArq(arquivo);
                movP++;
                
            }
            seekArq(j+1);
            movP++;
            aux.gravaNoArq(arquivo);
        }
    }
    
    public void selecaoDireta() throws IOException{
        Registro auxj = new Registro();
        Registro auxi = new Registro();
        Registro Menor = new Registro();
        int posMenor;
        for (int i = 0; i < filesize(); i++) {
            seekArq(i);
            auxi.leDoArq(arquivo);
            posMenor = i;
            seekArq(i);
            Menor.leDoArq(arquivo);
            for (int j = i+1; j < filesize(); j++) {
                seekArq(j);
                auxj.leDoArq(arquivo);
                if(auxj.getCodigo() < Menor.getCodigo()){
                    compP++;
                    posMenor = j;
                    seekArq(j);
                    Menor.leDoArq(arquivo);
                }
            }
            seekArq(i);
            Menor.gravaNoArq(arquivo);
            seekArq(posMenor);
            auxi.gravaNoArq(arquivo);
            movP+=2;
        }
    }
    
    public void bolha() throws IOException{
        Registro auxi = new Registro();
        Registro auxj = new Registro();
        for (int i = 0; i < filesize() - 1; i++) {
            for (int j = 0; j < filesize()- i - 1; j++) {
                seekArq(j);
                auxi.leDoArq(arquivo);
                seekArq(j+1);
                auxj.leDoArq(arquivo);
                if(auxi.getCodigo() > auxj.getCodigo()){
                    compP++;
                    movP+=2;
                    seekArq(j);
                    auxj.gravaNoArq(arquivo);
                    auxi.gravaNoArq(arquivo);
                }
            }
        }
    }
    
    public void shake() throws IOException{
        Registro auxi = new Registro();
        Registro auxj = new Registro();
        int inicio = 0,fim = (int) filesize();
        while(fim > inicio+1) {
            for (int j = inicio; j < fim - 1; j++) {
                seekArq(j);
                auxi.leDoArq(arquivo);
                auxj.leDoArq(arquivo);
                if(auxi.getCodigo() > auxj.getCodigo()){
                    compP++;
                    movP+=2;
                    seekArq(j);
                    auxj.gravaNoArq(arquivo);
                    auxi.gravaNoArq(arquivo);
                }
            }
            fim--;
            for (int j = fim-1; j >= inicio ; j--) {
                seekArq(j);
                auxi.leDoArq(arquivo);
                auxj.leDoArq(arquivo);
                if(auxi.getCodigo() > auxj.getCodigo()){
                    compP++;
                    movP+=2;
                    seekArq(j);
                    auxj.gravaNoArq(arquivo);
                    auxi.gravaNoArq(arquivo);
                }
            }
            inicio++;
        }
    }
    
    public void swap(Registro a1,int posa, Registro b1,int posb){
        seekArq(posa);
        a1.leDoArq(arquivo);
        seekArq(posb);
        b1.leDoArq(arquivo);
        seekArq(posa);
        b1.gravaNoArq(arquivo);
        seekArq(posb);
        a1.gravaNoArq(arquivo);
        movP+=2;
    }
    
    public void quickSort(){
        qSort(0, (int) filesize());
    }
    public void qSort(int low, int high)
    {
        if (low < high)
        {
            /* pi is partitioning index, arr[pi] is
              now at right place */
            int pi = Qparticao(low, high);
            // Recursively sort elements before
            // partition and after partition
            qSort(low, pi-1);
            qSort(pi+1, high);
        }
    }
    
    public int Qparticao(int min,int max){
        Registro auxi = new Registro();
        Registro auxj = new Registro();
        Registro auxk = new Registro();
        int p = min-1;
        seekArq(max);
        auxj.leDoArq(arquivo);
        for (int j = min; j <= max - 1; j++){
            seekArq(j);
            auxi.leDoArq(arquivo);
            if(auxi.getCodigo() <= auxj.getCodigo()){
                compP++;
                p++;
                swap(auxi,j,auxk,p);
            }
        }
        p++;
        seekArq(max);
        swap(auxi,p,auxj,max);
        return p;
    }
    
    public void merge() throws IOException{
        Arquivo_Java arq1 = new Arquivo_Java("c:\\arquivo.dat");
        Arquivo_Java arq2 = new Arquivo_Java("c:\\arquivo.dat");
        Registro r1 = new Registro();
        Registro r2 = new Registro();
        int seq = 1;
        while(seq<arquivo.length()){
            MParticao(r1,arq1,r2,arq2);
            fusao(r1,arq1,r2,arq2,seq);
            seq = seq*2;
        }
    }
    
    public void MParticao(Registro r1,Arquivo_Java arq1,Registro r2,Arquivo_Java arq2) throws IOException{
        for (int i = 0,j = (int) (arquivo.length()/2+1); (i<arquivo.length()/2); i++,j++) {
            seekArq(i);
            r1.leDoArq(arquivo);
            r1.gravaNoArq(arq1.arquivo);
            seekArq(j);
            r2.leDoArq(arquivo);
            r2.gravaNoArq(arq2.arquivo);
        }
    }

    private void fusao(Registro r1, Arquivo_Java arq1, Registro r2, Arquivo_Java arq2, int seq) {
        
    }
    
    public void ShellSort() throws IOException
    {
        int n = (int) filesize();
        for (int gap = n/2; gap > 0; gap = gap/2)
        {
            for (int i = gap; i < n; i += 1)
            {
                Registro Temp = new Registro();
                seekArq(i);
                Temp.leDoArq(arquivo);
                int j = i;
                Registro aux1 = new Registro();
                seekArq(j - gap);
                aux1.leDoArq(arquivo);
                for (; j >= gap && aux1.getCodigo() > Temp.getCodigo(); j -= gap){
                    compP++;
                    movP+=2;
                    seekArq(j - gap);
                    aux1.leDoArq(arquivo);
                    seekArq(j);
                    aux1.gravaNoArq(arquivo);
                }
                seekArq(j);
                Temp.gravaNoArq(arquivo);
                movP++;
            }
        }
    }
    
    public void shellSort() {
        int h = 1;
        int n = (int) filesize();
        while(h < n)
                h = h * 3 + 1;
        h = h / 3;
        int c, j;
        while (h > 0) {
            for (int i = h; i < n; i++) {
                Registro aux = new Registro();
                Registro aux1 = new Registro();
                seekArq(i);
                aux.leDoArq(arquivo);
                j = i;
                seekArq(j-h);
                aux1.leDoArq(arquivo);
                while (j >= h && aux1.getCodigo() > aux.getCodigo()) {
                    seekArq(j-h);
                    aux1.leDoArq(arquivo);
                    seekArq(j);
                    aux1.gravaNoArq(arquivo);
                    j = j - h;
                }
                seekArq(j);
                aux.gravaNoArq(arquivo);
            }
            h = h / 2;
        }
    }
    
     void merge(int l, int m, int r)
    {
        // Find sizes of two subarrays to be merged
        int n1 = m - l + 1;
        int n2 = r - m;
 
        /* Create temp arrays */
        Registro[] r1=new Registro[n1];
        Registro[] r2=new Registro[n2];
        /*Copy data to temp arrays*/
        for (int i=0; i<n1; ++i){
            r1[i] = new Registro();
            seekArq(i);
            r1[i].leDoArq(arquivo);
        }
        for (int j=0; j<n2; ++j){
            r2[j] = new Registro();
            seekArq(j);
            r2[j].leDoArq(arquivo);
        }
 
 
        /* Merge the temp arrays */
 
        // Initial indexes of first and second subarrays
        int i = 0, j = 0;
 
        // Initial index of merged subarry array
        int k = l;
        while (i < n1 && j < n2)
        {
            
            if (r1[i].getCodigo() <= r2[j].getCodigo())
            {
                compP++; 
                seekArq(k);
                r1[i].gravaNoArq(arquivo);
                i++;
            }
            else
            {
                seekArq(k);
                r2[i].gravaNoArq(arquivo);
                j++;
            }
            k++;
        }
 
        /* Copy remaining elements of L[] if any */
        while (i < n1)
        {
            seekArq(k);
            r1[i].gravaNoArq(arquivo);
            i++;
            k++;
        }
 
        /* Copy remaining elements of L[] if any */
        while (j < n2)
        {
            seekArq(k);
            r2[j].gravaNoArq(arquivo);
            j++;
            k++;
        }
    }
 
    // Main function that sorts arr[l..r] using
    // merge()
    public void mergeSort(){
        Msort(0, (int) filesize());
    }
    void Msort(int l, int r)
    {
        if (l < r)
        {
            // Find the middle point
            int m = (l+r)/2;
 
            // Sort first and second halves
            Msort(l, m);
            Msort(m+1, r);
 
            // Merge the sorted halves
            merge(l, m, r);
        }
    }   

    public int buscaBinaria(Registro item, int low, int high) {
        Registro pos = new Registro();
        if (high <= low){
            seekArq(low);
            pos.leDoArq(arquivo);
            return (item.getCodigo() > pos.getCodigo())?  (low + 1): low;
        }
        int mid = (low + high)/2;
        seekArq(mid);
        pos.leDoArq(arquivo);
        if(item.getCodigo() == pos.getCodigo())
            return mid+1;

        if(item.getCodigo() > pos.getCodigo())
            return buscaBinaria(item, mid+1, high);
        return buscaBinaria(item, low, mid-1);
    }
}
