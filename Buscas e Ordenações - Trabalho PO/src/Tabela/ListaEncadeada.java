/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tabela;

import java.io.IOException;
import java.io.RandomAccessFile;
import Tabela.Arquivo_Java;
import Tabela.Registro;

/**
 *
 * @author andre
 */
class no{
    private no ant, prox;
    int info;

    public no(no ant, no prox, int info) {
        this.ant = ant;
        this.prox = prox;
        this.info = info;
    }

    public no getAnt() {
        return ant;
    }

    public no getProx() {
        return prox;
    }

    public int getInfo() {
        return info;
    }

    public void setAnt(no ant) {
        this.ant = ant;
    }

    public void setProx(no prox) {
        this.prox = prox;
    }

    public void setInfo(int info) {
        this.info = info;
    }
   
}

public class ListaEncadeada{
    protected no inicio, fim;
    
    public void insereInicio(int info){
        no aux = new no(null,inicio,info);
        if(inicio == null)
            inicio = fim = aux;
        else{
            inicio.setAnt(aux);
            inicio = aux;
        }
    }
    
    public no busca_Exaustiva(int info){
        no aux = inicio;
        while(aux != null&&aux.getInfo() != info) {
            aux = aux.getProx();
        }
        return aux!=null ? aux : null;
    }
    
    public void excluir(int info){
        no aux = busca_Exaustiva(info);
        
        if(aux != null){
            if(inicio == fim)
                this.init();
            else if(inicio == aux){
                inicio = aux.getProx();
                inicio.setAnt(null);
            }else if(fim == aux){
               fim = fim.getAnt();
               fim.setProx(null);
            }else{
                aux.getAnt().setProx(aux.getProx());
                aux.getProx().setAnt(aux.getAnt());
            }
        }
    }
    
    public void exibe(){
        no aux = inicio;
        while(aux != null){
            System.out.println(aux.getInfo()+",");
            aux = aux.getProx();
        }
    }
    
    public void init(){
        inicio = fim = null;
    }
    
}

class ordenacao extends ListaEncadeada{
    private RandomAccessFile arquivo;
    public void insercaoDireta(){
        no pos = inicio.getProx();
        no ppos;
        int aux;
        while(pos != null){
            aux = pos.getInfo();
            ppos = pos;
            while(ppos != inicio && aux < ppos.getAnt().getInfo()){
                ppos.setInfo(ppos.getAnt().getInfo());
                ppos = ppos.getAnt();
            }
            pos.setInfo(aux);
            pos = pos.getProx();
        }
    }
    public void selecaoDireta(){
        no menor,auxj,auxi = inicio;
        int aux;
        while(auxi != fim.getAnt()){
            menor = auxi;
            auxj = auxi.getProx();
            while(auxj != fim){
                if(auxj.getInfo() < menor.getInfo()){
                    menor = auxj;
                }
                auxj = auxj.getProx();
            }
            aux = auxi.getInfo();
            auxi.setInfo(menor.getInfo());
            menor.setInfo(aux);
            auxi = auxi.getProx();
        }
    }
    
    public void bolha(){
        no aux2,aux;
        int elem;
        aux = fim;
        while(aux != inicio.getProx()){
            aux2 = inicio;
            while(aux2 != fim.getAnt()){
                if(aux2.getInfo()<aux2.getProx().getInfo()){
                    elem = aux2.getInfo();
                    aux2.setInfo(aux2.getProx().getInfo());
                    aux2.getProx().setInfo(elem);
                }
                aux2 = aux2.getProx();
            }
            aux = aux.getAnt();
        }
    }
    
    public void shake(){
        no Auxinicio,Auxfinal,aux;
        int elem;
        Auxinicio = inicio;
        Auxfinal = fim;
        while(Auxinicio.getProx() != Auxfinal){
            aux = inicio;
            while(aux != fim.getAnt()){
                if(aux.getInfo()<aux.getProx().getInfo()){
                    elem = aux.getInfo();
                    aux.setInfo(aux.getProx().getInfo());
                    aux.getProx().setInfo(elem);
                }
                aux = aux.getProx();
                Auxfinal = Auxfinal.getAnt();
            }
            aux = fim;
            while(aux != inicio){
                if(aux.getInfo()<aux.getAnt().getInfo()){
                    elem = aux.getInfo();
                    aux.setInfo(aux.getAnt().getInfo());
                    aux.getAnt().setInfo(elem);
                }
                Auxinicio = Auxinicio.getProx();
            }
        }
    }
}

