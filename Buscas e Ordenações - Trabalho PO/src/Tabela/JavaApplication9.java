/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tabela;

import Tabela.Principal;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author André Hallwas Ribeiro Alves
 */
public class JavaApplication9 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /////ordenacao_Arquivo arq = new ordenacao_Arquivo("arquivo.dat");
        ////arq.geraArqRandomico();
        try {
            //////arq.bolha();
            //////arq.selecaoDireta();
            /////arq.insercaoDireta();
            /////arq.shake();
            /////arq.ShellSort();
            /////arq.quickSort();
            /////arq.mergeSort();
            /////arq.exibirArq();
            Principal p = new Principal("arquivo.dat");
            p.geraTabela();
            /////p.bolha();
            /////arq.copiarArquivo("arquivo10.dat");
        } catch (Exception ex) {
            Logger.getLogger(JavaApplication9.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
