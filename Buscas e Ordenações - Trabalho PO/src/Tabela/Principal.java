/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tabela;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Principal {

    ordenacao_Arquivo arqOrd, arqRev, arqRand, auxRev, auxRand;
    String caminho;
    PrintWriter gravarArq;
    FileWriter arq;
    File auxF;
    long tini,tfim,ttotal;

    public Principal(String caminho) {
        try {
            arq = new FileWriter("Tabela.txt");
            gravarArq = new PrintWriter(arq);
            this.caminho = caminho;
            
        } catch (IOException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    public void deleta(Arquivo_Java arq) throws IOException{
        auxF = new File(arq.nomearquivo);
        arq.arquivo.close();
        auxF.delete();
    }
    public void bolha() throws IOException{
        
        arqOrd.geraArqOrdenado();
        tini= System.currentTimeMillis();
        arqOrd.bolha();
        tfim= System.currentTimeMillis();
        arqOrd.Tempo = tfim-tini;
        deleta(arqOrd);
        
        
        arqRev.geraArqReverso();
        tini= System.currentTimeMillis();
        arqRev.bolha();
        tfim= System.currentTimeMillis();
        arqRev.Tempo = tfim-tini;
        deleta(arqRev);
        
        arqRand.geraArqRandomico();
        tini= System.currentTimeMillis();
        arqRand.bolha();
        tfim= System.currentTimeMillis();
        arqRand.Tempo = tfim-tini;
        gravaLinhaTabela(arqOrd,arqRev,arqRand);
        deleta(arqRand);
    }
    
     public void shell() throws IOException{
        arqOrd.geraArqOrdenado();
        tini= System.currentTimeMillis();
        arqOrd.shellSort();
        tfim= System.currentTimeMillis();
        arqOrd.Tempo = tfim-tini;
        deleta(arqOrd);
        
        arqRev.geraArqReverso();
        tini= System.currentTimeMillis();
        arqRev.ShellSort();
        tfim= System.currentTimeMillis();
        arqRev.Tempo = tfim-tini;
        deleta(arqRev);
        
        arqRand.geraArqRandomico();
        tini= System.currentTimeMillis();
        arqRand.ShellSort();
        tfim= System.currentTimeMillis();
        arqRand.Tempo = tfim-tini;
        gravaLinhaTabela(arqOrd,arqRev,arqRand);
        deleta(arqRand);
        
    }
     
     public void shake() throws IOException{
        arqOrd.geraArqOrdenado();
        tini= System.currentTimeMillis();
        arqOrd.shake();
        tfim= System.currentTimeMillis();
        arqOrd.Tempo = tfim-tini;
        deleta(arqOrd);
        
        arqRev.geraArqReverso();
        tini= System.currentTimeMillis();
        arqRev.shake();
        tfim= System.currentTimeMillis();
        arqRev.Tempo = tfim-tini;
        deleta(arqRev);
        
        arqRand.geraArqRandomico();
        tini= System.currentTimeMillis();
        arqRand.shake();
        tfim= System.currentTimeMillis();
        arqRand.Tempo = tfim-tini;
        gravaLinhaTabela(arqOrd,arqRev,arqRand);
        deleta(arqRand);
        
        
    }
     
     public void selecaoDireta() throws IOException{
        arqOrd.geraArqOrdenado();
        tini= System.currentTimeMillis();
        arqOrd.selecaoDireta();
        tfim= System.currentTimeMillis();
        arqOrd.Tempo = tfim-tini;
        deleta(arqOrd);
        
        arqRev.geraArqReverso();
        tini= System.currentTimeMillis();
        arqRev.selecaoDireta();
        tfim= System.currentTimeMillis();
        arqRev.Tempo = tfim-tini;
        deleta(arqRev);
        
        arqRand.geraArqRandomico();
        tini= System.currentTimeMillis();
        arqRand.selecaoDireta();
        tfim= System.currentTimeMillis();
        arqRand.Tempo = tfim-tini;
        gravaLinhaTabela(arqOrd,arqRev,arqRand);
        deleta(arqRand);
        
    }
     
     public void insercaoDireta() throws IOException{
        arqOrd.geraArqOrdenado();
        tini= System.currentTimeMillis();
        arqOrd.insercaoDireta();
        tfim= System.currentTimeMillis();
        arqOrd.Tempo = tfim-tini;
        deleta(arqOrd);
        
        arqRev.geraArqReverso();
        tini= System.currentTimeMillis();
        arqRev.insercaoDireta();
        tfim= System.currentTimeMillis();
        arqRev.Tempo = tfim-tini;
        deleta(arqRev);
        
        arqRand.geraArqRandomico();
        tini= System.currentTimeMillis();
        arqRand.insercaoDireta();
        tfim= System.currentTimeMillis();
        arqRand.Tempo = tfim-tini;
        gravaLinhaTabela(arqOrd,arqRev,arqRand);
        deleta(arqRand);
        
    }
     
    public void quick() throws IOException{
        arqOrd.geraArqOrdenado();
        tini= System.currentTimeMillis();
        arqOrd.quickSort();
        tfim= System.currentTimeMillis();
        arqOrd.Tempo = tfim-tini;
        deleta(arqOrd);
        
        arqRev.geraArqReverso();
        tini= System.currentTimeMillis();
        arqRev.quickSort();
        tfim= System.currentTimeMillis();
        arqRev.Tempo = tfim-tini;
        deleta(arqRev);
        
        arqRand.geraArqRandomico();
        tini= System.currentTimeMillis();
        arqRand.quickSort();
        tfim= System.currentTimeMillis();
        arqRand.Tempo = tfim-tini;
        gravaLinhaTabela(arqOrd,arqRev,arqRand);
        deleta(arqRand);
        
        
    }
    
    public void merge() throws IOException{
        arqOrd.geraArqOrdenado();
        tini= System.currentTimeMillis();
        arqOrd.mergeSort();
        tfim= System.currentTimeMillis();
        arqOrd.Tempo = tfim-tini;
        deleta(arqOrd);
        
        arqRev.geraArqReverso();
        tini= System.currentTimeMillis();
        arqRev.mergeSort();
        tfim= System.currentTimeMillis();
        arqRev.Tempo = tfim-tini;
        deleta(arqRev);
        
        arqRand.geraArqRandomico();
        tini= System.currentTimeMillis();
        arqRand.mergeSort();
        tfim= System.currentTimeMillis();
        arqRand.Tempo = tfim-tini;
        gravaLinhaTabela(arqOrd,arqRev,arqRand);
        deleta(arqRand);
        
        
    }
    
    public void heap() throws IOException{
        
        arqOrd.geraArqOrdenado();
        tini= System.currentTimeMillis();
        arqOrd.mergeSort();
        tfim= System.currentTimeMillis();
        arqOrd.Tempo = tfim-tini;
        deleta(arqOrd);
        
        arqRev.geraArqOrdenado();
        tini= System.currentTimeMillis();
        arqRev.mergeSort();
        tfim= System.currentTimeMillis();
        arqRev.Tempo = tfim-tini;
        deleta(arqRev);
        
        arqRand.geraArqOrdenado();
        tini= System.currentTimeMillis();
        arqRand.mergeSort();
        tfim= System.currentTimeMillis();
        arqRand.Tempo = tfim-tini;
        gravaLinhaTabela(arqOrd,arqRev,arqRand);
        deleta(arqRand);
        
    } 
     
    public void gravaLinhaTabela(ordenacao_Arquivo arq,ordenacao_Arquivo arq1,ordenacao_Arquivo arq2){
        gravarArq.printf("%10d %13d %12d %10d %7d ",arq.compP,arq.compEq,arq.movP,arq.movEq,arq.Tempo/1000);
        gravarArq.printf("%10d %13d %12d %10d %7d ",arq1.compP,arq1.compEq,arq1.movP,arq1.movEq,arq1.Tempo/1000);
        gravarArq.printf("%10d %13d %12d %10d %7d %n",arq2.compP,arq2.compEq,arq2.movP,arq2.movEq,arq2.Tempo/1000);
        /////gravarArq.print("          "+arq.compP+"              "+arq.compEq+"              "+arq.movP+"              "+arq.movEq+"        "+arq.Tempo+"  ");
        /////gravarArq.print("          "+arq1.compP+"              "+arq1.compEq+"             "+arq1.movP+"             "+arq1.movEq+"        "+arq1.Tempo/1000+"  ");
        /////gravarArq.println("          "+arq2.compP+"              "+arq2.compEq+"             "+arq2.movP+"             "+arq2.movEq+"        "+arq2.Tempo/1000+"  ");
    }
    public void geraNovo(){
        arqOrd = new ordenacao_Arquivo("arquivo.dat");
        arqRev = new ordenacao_Arquivo("arquivo1.dat");
        arqRand = new ordenacao_Arquivo("arquivo2.dat");
    }
    public void geraTabela(){
        try {
            arq = new FileWriter("Tabela.txt");
            gravarArq = new PrintWriter(arq);
            gravarArq.println("Métodos Ordenação"+"   "
                             +"Arquivo Ordenado"+"                                 "
                             +"Arquivo em Ordem Reversa"+"                                               "
                             +"Arquivo Randômico");
            gravarArq.println(" Comp. Prog.  "+"Comp. Equa.  " +"Mov. Prog.  "+"Mov. Equa.  "+"Tempo  "+"Comp.Prog.  "+
            "Comp. Equa.  "+"Mov. Prog.  "+"Mov. Equa.  "+"Tempo   "+"Comp.Prog.  "+"Comp. Equa.  "+"Mov. Prog.  "+"Mov. Equa. "+
            "Tempo");
            geraNovo();
            insercaoDireta();
            geraNovo();
            insercaoDiretaBinaria();
            geraNovo();
            selecaoDireta();
            geraNovo();
            bolha();
            geraNovo();
            shake();
            geraNovo();
            shell();
            /////heap();
            geraNovo();
            quick();
            geraNovo();
            merge();
            arq.close();
        } catch (Exception ex) 
        {    }
    }

    private void insercaoDiretaBinaria() throws IOException {
        arqOrd.geraArqOrdenado();
        tini= System.currentTimeMillis();
        arqOrd.insercaoDiretaBinaria();
        tfim= System.currentTimeMillis();
        arqOrd.Tempo = tfim-tini;
        deleta(arqOrd);
        
        arqRev.geraArqOrdenado();
        tini= System.currentTimeMillis();
        arqRev.insercaoDiretaBinaria();
        tfim= System.currentTimeMillis();
        arqRev.Tempo = tfim-tini;
        deleta(arqRev);
        
        arqRand.geraArqOrdenado();
        tini= System.currentTimeMillis();
        arqRand.insercaoDiretaBinaria();
        tfim= System.currentTimeMillis();
        arqRand.Tempo = tfim-tini;
        gravaLinhaTabela(arqOrd,arqRev,arqRand);
        deleta(arqRand);
    }
}

