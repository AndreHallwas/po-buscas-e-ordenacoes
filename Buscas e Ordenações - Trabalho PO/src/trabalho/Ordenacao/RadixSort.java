/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho.Ordenacao;

import javafx.scene.canvas.GraphicsContext;
import trabalho.Desenho.Desenha;

/**
 *
 * @author andre
 */
public class RadixSort {

    public static void radixSort(int vector[]) {
        for (int digit = 0; digit < 3; digit++) {
            int power = (int) Math.pow(10, digit + 1);

            int z[][] = new int[vector.length][10];
            int n[] = new int[10];

            for (int i = 0; i < vector.length; i++) {
                int num = vector[i];
                z[n[(num % power) / (power / 10)]][(num % power) / (power / 10)] = num;
                n[(num % power) / (power / 10)]++;

            }
            int c = 0;
            for (int i = 0; i < 10; i++) {

                for (int j = 0; j < vector.length; j++) {
                    if (j < n[i]) {
                        vector[c] = z[j][i];
                        c++;
                    } else {
                        break;
                    }
                }
            }

        }
    }
    public static void radixSortDesenho(Desenha d) throws InterruptedException {
        Desenha a = new Desenha(d.getCtx());
        for (int digit = 0; digit < 3; digit++) {
            int power = (int) Math.pow(10, digit + 1);

            int z[][] = new int[d.getVet().length][10];
            int n[] = new int[10];

            for (int i = 0; i < d.getVet().length; i++) {
                int num = d.getVet()[i];
                z[n[(num % power) / (power / 10)]][(num % power) / (power / 10)] = num;
                n[(num % power) / (power / 10)]++;

            }
            int c = 0;
            for (int i = 0; i < 10; i++) {

                for (int j = 0; j < d.getVet().length; j++) {
                    if (j < n[i]) {
                        d.getVet()[c] = z[j][i];
                        /////d.setVet(d.getVet());
                        d.ThreadExibe();
                        Thread.sleep(80);
                        c++;
                    } else {
                        break;
                    }
                }
            }
        }
    }
    
    public static void ThreadRadix(Desenha d){
       Thread threadRadix = new Thread(){
           public void run() {
               try {
                   radixSortDesenho(d);
               } catch (Exception e) {
                   System.out.println(e);
               }
           } 
        };
        threadRadix.start();
    }
}
