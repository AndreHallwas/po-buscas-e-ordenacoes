/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho.Ordenacao;

import trabalho.Desenho.Desenha;

public class BucketSort{
 
   public static void sort(Desenha d, int maxVal) throws InterruptedException {
      int [] bucket=new int[maxVal+1];
 
      for (int i=0; i<bucket.length; i++) {
         bucket[i]=0;
      }
 
      for (int i=0; i<d.getVet().length; i++) {
         bucket[d.getVet()[i]]++;
      }
 
      int outPos=0;
      for (int i=0; i<bucket.length; i++) {
         for (int j=0; j<bucket[i]; j++) {
            d.getVet()[outPos++]=i;
            d.ThreadExibe();
            Thread.sleep(100);
         }
      }
   }
   
   
    public static void ThreadBucket(Desenha d,int MaxVal){
       Thread threadBucket = new Thread(){
           public void run() {
               try {
                   sort(d,MaxVal);
               } catch (Exception e) {
                   System.out.println(e);
               }
           } 
        };
        threadBucket.start();
    }
 
   /*
   public static void main(String[] args) {
      int maxVal=5;
      int [] data= {5,3,0,2,4,1,0,5,2,3,1,4}; 
 
      System.out.println("Before: " + Arrays.toString(data));
      sort(data,maxVal);
      System.out.println("After:  " + Arrays.toString(data));
   }*/
}