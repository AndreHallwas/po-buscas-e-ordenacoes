/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho.Ordenacao;

import trabalho.Desenho.Desenha;

public class CountingSort {

    public static void CountingSort(int[] array, int leftIndex, int rightIndex) {

        //Encontrar o maior valor 
        int k = 0;
        for (int m = leftIndex; m < rightIndex; m++) {
            if (array[m] > k) {
                k = array[m];
            }
        }

        //Cria vetor com o tamanho do maior elemento
        int[] vetorTemporario = new int[k];

        //Inicializar com zero o vetor temporario
        for (int i = 0; i < vetorTemporario.length; i++) {
            vetorTemporario[i] = 0;
        }

        //Contagem das ocorrencias no vetor desordenado
        for (int j = leftIndex; j < rightIndex; j++) {
            vetorTemporario[array[j]] += 1;
        }

        //Fazendo o  complemento do numero anterior 
        for (int i = leftIndex; i < rightIndex; i++) {
            vetorTemporario[i] = vetorTemporario[i] + vetorTemporario[i - 1];
        }

        //Ordenando o array da direita para a esquerda
        int[] vetorAuxiliar = new int[array.length];
        for (int j = rightIndex; j > leftIndex; j--) {
            vetorAuxiliar[vetorTemporario[array[j]]] = array[j];
            vetorTemporario[array[j]] -= 1;
        }

        //Retornando os valores ordenados para o vetor de entrada
        for (int i = leftIndex; i < rightIndex; i++) {
            array[i] = vetorAuxiliar[i];
        }
    }
    
    public static void ordena(Desenha d, int m) throws InterruptedException{
        int n = d.getVet().length;
         
        int vetorAuxiliar[] = new int[m];
         
        //1ª - (Inicializar com zero)
        for(int i = 0; i < m; i++){
            vetorAuxiliar[i] = 0;
        }
         
        //2ª - Contagem das ocorrencias
        for(int i = 0; i < n; i++){
            vetorAuxiliar[d.getVet()[i]]++;
        }
 
        //3ª - Ordenando os indices do vetor auxiliar
        int sum = 0;                
        for(int i = 1; i < m; i++){
            int dum = vetorAuxiliar[i];
            vetorAuxiliar[i] = sum;
            /////d.ThreadExibe(vetorAuxiliar);
            /////Thread.sleep(100);
            sum += dum;
            d.ThreadExibe(vetorAuxiliar);
            Thread.sleep(100);
        }       
        int vetorOrdenado[] = new int[n];
        for(int i = 0; i < n; i++){
            vetorOrdenado[vetorAuxiliar[d.getVet()[i]]] = d.getVet()[i];
            ////////////d.ThreadExibe(vetorOrdenado);
            ////////////Thread.sleep(100);
            vetorAuxiliar[d.getVet()[i]]++;
            d.ThreadExibe(vetorOrdenado);
            Thread.sleep(100);
        }
         
        //4ª Retornando os valores ordenados para o vetor de entrada
        for (int i = 0; i < n; i++){
            d.getVet()[i] = vetorOrdenado[i];
            d.ThreadExibe();
            Thread.sleep(100);
        }
    }
    public static void ThreadCounting(Desenha d,int MaxVal){
       Thread threadCounting = new Thread(){
           public void run() {
               try {
                   ordena(d,MaxVal);
               } catch (Exception e) {
                   System.out.println(e);
               }
           } 
        };
        threadCounting.start();
    }
}
