/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho.ui;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.HBox;
import trabalho.Desenho.Desenha;
import trabalho.Ordenacao.BucketSort;
import trabalho.Ordenacao.CountingSort;
import trabalho.Ordenacao.RadixSort;

/**
 * FXML Controller class
 *
 * @author andre
 */
public class TrabalhoPoController implements Initializable {

    @FXML
    private HBox hbCanvas;
    @FXML
    private Canvas CDesenhar;
    @FXML
    private Canvas CV1;
    @FXML
    private Canvas CV2;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        paintComponent();
    }    
    
    public void paintComponent(){
        GraphicsContext ctx = CDesenhar.getGraphicsContext2D();
        Desenha d = new Desenha(ctx);
        Desenha d1 = new Desenha(CV1.getGraphicsContext2D());
        Desenha d2 = new Desenha(CV2.getGraphicsContext2D());
        RadixSort.ThreadRadix(d1);
        BucketSort.ThreadBucket(d2, 256);
        CountingSort.ThreadCounting(d,256);
        //////d.ThreadLoop();
    }
    
}
