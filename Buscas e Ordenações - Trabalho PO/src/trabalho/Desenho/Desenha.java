/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho.Desenho;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 *
 * @author andre
 */
public class Desenha{
    private GraphicsContext ctx;
    private int vet[];
    private int Largura;
    private int meiodaTela;
    private Timer timer1;
    private Timer timer2;
    private Thread threadExibe;
    private Thread threadLoop;
    private double fator;
    
    public Desenha(GraphicsContext ctx){
        this.ctx = ctx;
        this.Largura = 4;
        this.meiodaTela = 150;
        ctx.setLineWidth(2);
        Clear();
        geraVetor(200);
        calculaFator();
    }
    
    public Desenha(GraphicsContext ctx,int Largura){
        this.ctx = ctx;
        this.Largura = Largura;
        this.meiodaTela = 300;
        ctx.setLineWidth(2);
        Clear();
        geraVetor(200);
        calculaFator();
    }
    
    public Desenha(GraphicsContext ctx,int Largura,int meiodaTela){
        this.ctx = ctx;
        this.Largura = Largura;
        this.meiodaTela = meiodaTela;
        ctx.setLineWidth(2);
        Clear();
        geraVetor(200);
        calculaFator();
        /////int vet[]
    }
    
    public Desenha(GraphicsContext ctx,int Largura,int meiodaTela,int vet[]){
        this.ctx = ctx;
        this.Largura = Largura;
        this.meiodaTela = meiodaTela;
        this.vet = vet;
        ctx.setLineWidth(2);
        Clear();
        geraVetor(200);
        calculaFator();
    }
    
    public void DesenhaVetor() {
        ///////////////geraVetor(150);/////
        ctx.setFill(Color.BLACK);
        ctx.fillRect(0, 0, vet.length*Largura*1.58,300 );
        ctx.setFill(Color.GREENYELLOW);
        for(int i = 5,j = 0;j<vet.length;i+=Largura*fator,j++){/////1.5
            ctx.fillRect(i, meiodaTela-vet[j], Largura, vet[j]);/////largura*fator
            ctx.strokeRect(i, meiodaTela-vet[j], Largura, vet[j]);/////largura*fator
        }
        ctx.stroke();
        ctx.fill();
    }
    public void DesenhaVetor(int vetor[]) {
        ///////////////geraVetor(150);/////
        ctx.setFill(Color.BLACK);
        ctx.fillRect(0, 0, vetor.length*Largura*1.58,300 );
        ctx.setFill(Color.GREENYELLOW);
        for(int i = 5,j = 0;j<vetor.length;i+=Largura*fator,j++){/////1.5
            ctx.fillRect(i, meiodaTela-vetor[j], Largura, vetor[j]);/////largura*fator
            ctx.strokeRect(i, meiodaTela-vetor[j], Largura, vetor[j]);/////largura*fator
        }
        ctx.stroke();
        ctx.fill();
    }
    
    public void Clear() {
        ctx.setFill(Color.BLACK);
        ctx.fillRect(0, 0, ctx.getCanvas().getWidth(), ctx.getCanvas().getHeight());
        ctx.stroke();
        ctx.fill();
    }
    public void calculaFator(){
        fator = ((ctx.getCanvas().getWidth()/vet.length)/2);
        if(vet.length <= 20)
            fator = fator*0.72;
    }
    public static void DesenhaVetor(GraphicsContext ctx,int vet[],int Largura) {
        ctx.setFill(Color.BLACK);
        ctx.fillRect(0, 0, vet.length*Largura*1.55,300 );
        ctx.setFill(Color.GREENYELLOW);
        for(int i = 5,j = 0;j<vet.length;i+=Largura*1.5,j++){
            ctx.fillRect(i, 300-vet[j], Largura, vet[j]);
            ctx.strokeRect(i, 300-vet[j], Largura, vet[j]);
        }
        ctx.stroke();
        ctx.fill();
    }
    public static void Clear(GraphicsContext ctx) {
        ctx.setFill(Color.BLACK);
        ctx.fillRect(0, 0, ctx.getCanvas().getWidth(), ctx.getCanvas().getHeight());
        ctx.stroke();
        ctx.fill();
    }
    
    public void geraVetor(int Tam){
        vet = new int[Tam];
        Random gerador = new Random();
        for(int i = 0;i<Tam;i++){
            vet[i] = gerador.nextInt(256);
        }
    }
     
    public void ThreadLoop(){
       threadLoop = new Thread(){
           public void run() {
               try {
                   while(true){
                      ThreadExibe(); 
                      Thread.sleep(200);
                   }
               } catch (Exception e) {
                   System.out.println(e);
               }
           } 
        };
        threadLoop.start();
    }
   
    public void ThreadExibe(){
        threadExibe = new Thread(){
           public void run() {
               try {
                    Clear();
                    DesenhaVetor();
               } catch (Exception e) {
                   System.out.println(e);
               }
           } 
        };
        threadExibe.start();
    }
    public void ThreadExibe(int vetor[]){
        threadExibe = new Thread(){
           public void run() {
               try {
                    Clear();
                    DesenhaVetor(vetor);
               } catch (Exception e) {
                   System.out.println(e);
               }
           } 
        };
        threadExibe.start();
    }
    
    public void Timer2(Timer timer){
        TimerTask tarefa = new TimerTask() {
            public void run() {
                try {
                    Clear();
                    DesenhaVetor();
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        };
        timer.scheduleAtFixedRate(tarefa, 500, 300);
    }
           
    public void CtrTimer2(){
        timer1 = new Timer();
        TimerTask tarefa = new TimerTask() {
            public void run() {
                try {
                    timer2 = new Timer();
                    Timer2(timer2);
                    Thread.sleep(20000);
                    timer2.cancel();
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        };
        timer1.scheduleAtFixedRate(tarefa, 0, 30000);
    }

    public GraphicsContext getCtx() {
        return ctx;
    }

    public int[] getVet() {
        return vet;
    }

    public int getLargura() {
        return Largura;
    }

    public int getMeiodaTela() {
        return meiodaTela;
    }

    public void setVet(int[] vet) {
        this.vet = vet;
    }

    public void setCtx(GraphicsContext ctx) {
        this.ctx = ctx;
    }

    public void setLargura(int Largura) {
        this.Largura = Largura;
    }

    public void setMeiodaTela(int meiodaTela) {
        this.meiodaTela = meiodaTela;
    }
    
}
